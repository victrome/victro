import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
//import Row from 'react-bootstrap/Row';
//import Col from 'react-bootstrap/Col';
import Menu from '../Menu/menu';
import style from './header.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prevScrollpos: 0,
      fixed: false,
      color : '',
      section: 1,
      mobilemenuOpen: false
    };
  }

  // Adds an event listener when the component is mount.
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  // Remove the event listener when the component is unmount.
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleMobileMenu = () => {
    const mobilemenuOpen  = !this.state.mobilemenuOpen;
    this.setState({
      mobilemenuOpen: mobilemenuOpen
      });
  }
  handleHideMobileMenu = () => {
    this.setState({mobilemenuOpen: false, section:0});
  }
  
  // Hide or show the menu.
  handleScroll = () => {
    const { prevScrollpos } = this.state;

    const currentScrollPos = window.pageYOffset;
    const fixed = prevScrollpos > window.innerHeight - 280;
    var color = 'blue';
    var section = 1;
    var extraTop = 60;
    //var home = document.getElementById('home').getBoundingClientRect().top - extraTop;
    var about = document.getElementById('about').getBoundingClientRect().top - extraTop;
    var portfolio = document.getElementById('portfolio').getBoundingClientRect().top - extraTop;
    var contact = document.getElementById('contact').getBoundingClientRect().top - extraTop;
  
    
    if(contact <= 0){
      color = 'purple';
      section = 4;
    } else if(portfolio <= 0){
      color = 'pink';
      section = 3;
    } else if(about <= 0){
      color = 'blue';
      section = 2;
    } else {
      section = 1;
    }
    
    

    this.setState({
      prevScrollpos: currentScrollPos,
      fixed: fixed,
      color: color,
      section: section
    });
  };
  

  render() {
    return (
      <div className={style['header']+" "+(this.state.fixed ? style['fixed'] : '')+" "+style[this.state.color]+" "+(this.state.mobilemenuOpen ? style["mobile-menu-opened"] : "")}>
        <Container>
            <div>
                <div className={style['header-logo']+" "+(this.state.fixed ? style['display'] : '')}></div>
                <Menu handleHideMobileMenu={this.handleHideMobileMenu} section={this.state.section}/>
                <div onClick={this.handleMobileMenu} className={style['menu-mobile']+" "+(this.state.mobilemenuOpen ? style[this.state.color] : '')}>
                  {this.state.mobilemenuOpen ? (
                    <FontAwesomeIcon icon={faTimes} />
                  ) : (
                    <FontAwesomeIcon icon={faBars} />
                  )}
                </div>
            </div>
          </Container>
        </div>
    );
  }
  }