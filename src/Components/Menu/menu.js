

import React, { Component } from "react";
import Container from 'react-bootstrap/Container';
import style from './menu.module.scss';
import data from '../../assets/data/menu.en'
import AnchorLink from 'react-anchor-link-smooth-scroll'

export default class Menu extends Component {
  constructor(props) {
    super(props);
  }
        
  

  render() {
    return (
      <ul className={style['menu']}>
            <li><AnchorLink href="#home" className={(this.props.section == 1 ? style['active'] : '')} onClick={this.props.handleHideMobileMenu}>{data.home}</AnchorLink></li>
            <li><AnchorLink href="#about" className={(this.props.section == 2 ? style['active'] : '')} onClick={this.props.handleHideMobileMenu}>{data.about}</AnchorLink></li>
            <li><AnchorLink href="#portfolio" className={(this.props.section == 3 ? style['active'] : '')} onClick={this.props.handleHideMobileMenu}>{data.portfolio}</AnchorLink></li>
            <li><AnchorLink href="#contact" className={(this.props.section == 4 ? style['active'] : '')}onClick={this.props.handleHideMobileMenu}>{data.contact}</AnchorLink></li>
          </ul>
    );
  }
  }