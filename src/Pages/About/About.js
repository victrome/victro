import React from 'react';
import style from './About.module.scss';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import data from '../../assets/data/about.en';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookOpen } from '@fortawesome/free-solid-svg-icons'
import { faFilePdf } from '@fortawesome/free-regular-svg-icons'

const about = () => {

    const getListSkill = () => {
        let skills = data.skillsList;
        return Object.entries(skills).map((item, j) => {
            let aval = [];
            const number = item[1].number/2;
            const empties = 5 - number;
            for(let i = 0; i < number; i++){
                aval.push(<div key={"skill-full"+i} className={style['skill-full']}></div>)
            }
            for(let i = 0; i < empties; i++){
                aval.push(<div key={"skill-empty"+i} className={style['skill-empty']}></div>)
            }
            return <Col md="4" key={"skill"+j} >{aval} <span className={style['skill-name']+" mt-2"}>{item[1].name}</span></Col>
        });    
    }
    const getListArticle = () => {
        let articles = data.articlesList;
        return Object.entries(articles).map((item, i) => {
            const titleSize = 2;
            return (
            <Row key={"article"+i} className={style['article']}>
                <Col xs={titleSize} className={style['title']}>
                    {data.article}
                </Col>
                <Col xs={12 - titleSize} className={style['name']+" pl-2"}>
                    {item[1].name}
                </Col>
                <Col md={9} xs={12}>
                    <Row>
                        <Col xs={2} className={style['title']}>
                            {data.authors}
                        </Col>
                        <Col xs={10} className={style['info']+" pl-5"} dangerouslySetInnerHTML={{__html: item[1].authors}}>
                            
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={2} className={style['title']}>
                            {data.published_on}
                        </Col>
                        <Col xs={10} className={style['info']+" pl-5"}>
                            {item[1].published_on}
                        </Col>
                    </Row>
                </Col>
                <Col md={3} xs={12} className="mt-3 mt-md-0">
                    <Row>
                        <Col xs={4} className="text-center p-0">
                            <div className={style['btn']}>
                                 <FontAwesomeIcon icon={faBookOpen} />
                            </div>
                            <div className={style['btn-line']}>
                                {data.website}
                            </div>
                        </Col>
                        <Col xs={4} className="text-center p-0">
                            <div className={style['btn']}>
                                <FontAwesomeIcon icon={faFilePdf} />
                            </div>
                            <div className={style['btn-line']}>
                                {data.pdf_pt}
                            </div>
                        </Col>
                        <Col xs={4} className="text-center p-0">
                            <div className={style['btn']}>
                                <FontAwesomeIcon icon={faFilePdf} />
                            </div>
                            <div className={style['btn-line']}>
                                {data.pdf_en}
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>)
        });    
    }
    return (
        <div className={style['About']+" pb-5"} id="about">
            <div className={style['blueLine']}>
                <div className={style['planets']}>
                    <div className={style['me']}>
                        <div>
                            <Container>
                                <div className={style['title']}>{data.title}</div>
                            </Container>
                        </div>
                    </div>
                </div>
            </div>
            <Container>
                <div className={style['bio']+" px-4"}>
                    {data.about_me}
                </div>
                <div>
                    <div className={style['title']}>{data.skills}</div>
                    <div className="px-4">
                        <Row>
                            {getListSkill()}
                            
                        </Row>
                    </div>
                </div>
                <div className="mt-5">
                    <div className={style['title']}>{data.published_skills}</div>
                    <div className="px-4">
                        {getListArticle()}
                    </div>
                        
                    
                </div>
            </Container>
        </div>
    )
}

export default about;