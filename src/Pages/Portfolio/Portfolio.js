import React from 'react';
import style from './Portfolio.module.scss';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import data from '../../assets/data/portfolio.en';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookOpen } from '@fortawesome/free-solid-svg-icons'
import { faFilePdf } from '@fortawesome/free-regular-svg-icons'

const portfolio = () => {
    const getListPortfolio = () => {
        let projects = data.portfolioList;
        return Object.entries(projects).map((item, j) => {
            return (
              <Col md="4" key={"portfolio"+j} >
                <Card body className="mb-5">{item[1].name}</Card>
              </Col>
            )
        });    
    }
    return (
        <div className={style['Portfolio']+" pb-5"} id="portfolio">
            <div className={style['pinkLine']}>
                <div className={style['icons']}>
                    <div className={style['center']}>
                        <div>
                            <Container>
                                <div className={style['title']}>{data.title}</div>
                            </Container>
                        </div>
                    </div>
                </div>
            </div>
            <Container>
                <Row className="mt-5">
                  {getListPortfolio()}
                </Row>
            </Container>
        </div>
    )
}

export default portfolio;