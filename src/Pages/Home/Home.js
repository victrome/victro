import React from 'react';
import style from './Home.module.css';
import data from '../../assets/data/home.en';
import Header from '../../Components/Header/header';

const home = () => {
    return ( 
    <div className={style['black-background']} id="home">
        <div className={style['icon-background']}>
            <Header mode="fixed"/>
            <div className={style['home-middle']}></div>
            <div className={style['home-logo']}></div>
            <div className={style['home-line']}></div>
            <div className={style['home-name']}>{data.name}</div>
            <div className={style['home-role']}>{data.role}</div>
        </div>
    </div>
    );
}

export default home;
